# README

## Notice

This is a fork of the original repo:

https://bitbucket.org/mehrad_mahmoudian/dream-prostate-cancer-challenge-q.1a/src/master/

The only differences are:
1. Addition of this README file
2. slightly changed repo description due to the character limit of Codeberg (255) and addition of the DOI

## Description

The article ["A predictive model of overall survival in patients with metastatic castration-resistant prostate cancer"](https://pubmed.ncbi.nlm.nih.gov/31231503/) which is published in 2016-11-16 contains an improvement to another machine learning methods we had. This development has been done solely by performing a systematic feature elimination/reduction and subsequently improving the signal to noise ratio. This article has set the foundation of my PhD thesis and showed me that handling the features and selecting the correct feature-set can drastically improve the performance of the model. It also showed that feature selection can help us understand the dynamics of the underlying phenomena better.

By the knowledge we got from the aforementioned study, I ultimately managed to create the [Stable Iterative Variable Selection](https://academic.oup.com/bioinformatics/article/37/24/4810/6322982?login=true) method to tackle the feature selection of relatively low-sample-size high dimentional data. Therefore, this study should be considered as a milestone :)
